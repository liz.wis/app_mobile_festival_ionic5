import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    public afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp(){
    
      this.afAuth.authState.subscribe(auth => {
        if(!auth){
          console.log('pas connecté(e)');
          this.router.navigateByUrl('/login');
        } else {
          console.log('connecté(e)');
          this.router.navigateByUrl('/tabs/tab1');
        }
      });
    }

    
  }

