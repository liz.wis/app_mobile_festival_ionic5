import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-music',
  templateUrl: './music.page.html',
  styleUrls: ['./music.page.scss'],
})

export class MusicPage implements OnInit {
  timer: any;

  constructor(public modalController: ModalController,
    private nativeAudio: NativeAudio) {
      this.readSound();
     }

  close(){
    this.modalController.dismiss();
  }

  

  stopMusic(){
    this.nativeAudio.stop('sound');
  }

  readSound(){
    this.nativeAudio.preloadSimple('sound', '../assets/sounds/reflechir.mp3')
    .then(onSuccess => {
      console.log(onSuccess);
      this.nativeAudio.play('sound');
    }, onError => {
      console.log(onError);
    });
  }

  ngOnInit() {
  }

}
