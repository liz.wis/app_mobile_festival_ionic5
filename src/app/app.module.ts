import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { MusicPageModule } from './music/music.module';

import { NativeAudio } from '@ionic-native/native-audio/ngx';

import { RecaptchaModule } from "ng-recaptcha";


export const firebaseConfig = {
  apiKey: "AIzaSyDvq9Ila_dAgn_G0YrT5tEQkH5oYWPEI7g",
  authDomain: "taidamb-e654d.firebaseapp.com",
  projectId: "taidamb-e654d",
  storageBucket: "taidamb-e654d.appspot.com",
  messagingSenderId: "782350544912",
  appId: "1:782350544912:web:db82559a973e97b31a1a95"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule, MusicPageModule, RecaptchaModule, AngularFirestoreModule],
  providers: [NativeAudio, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
