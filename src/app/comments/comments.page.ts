import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {
  showForm: boolean;
  firebaseData = {
    name: '',
    img: '',
    description: ''
  };
  comments: Observable<any[]>;

  constructor(public firestore: AngularFirestore) {
    this.Chargecomments();
   }


  add(){
    this.showForm = !this.showForm;
  }

  addFirebase(){
    this.firestore.collection('Commentary').add(this.firebaseData);
    this.showForm = !this.showForm;
    this.firebaseData = {
      name: '',
      img: '',
      description: ''
    };
  }

  Chargecomments(){
    this.comments = this.firestore.collection('Commentary').valueChanges();
  }

 

  ngOnInit() {
   
  }


}
