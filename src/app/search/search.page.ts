import { Component, OnInit } from '@angular/core';

import { MusicPage } from '../music/music.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  constructor(public modalController: ModalController) { }

  async Music(music: any) {
    const modal = await this.modalController.create({
      component: MusicPage,
      componentProps: music
    });
    return await modal.present();
  }

array = [
  {
    name: 'DaftPunk',
    img: '../assets/daftpunk.jpg'
  },
  {
    name: 'Booba',
    img: '../assets/booba.png'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg',
    sound: '../assets/sounds/reflechir.mp3'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg'
  },
  {
    name: 'Wejdene',
    img: '../assets/wejdene.jpg'
  },
]


  ngOnInit() {
  }

}
