import { Component } from '@angular/core';

import { GoogleMaps,
  GoogleMap,
  GoogleMapsMapTypeId,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';

import { ActionSheetController, Platform, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  map: GoogleMap;
 

  
  constructor(
    public alertController: AlertController,
    public actionCtrl: ActionSheetController,
    private platform: Platform
  ) {
    if(this.platform.is('cordova')){
      this.loadMap();
    }
  }

  loadMap(){
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDEx1CMpu0VsUSq5FNl67vqDeOTjND0SVA',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDEx1CMpu0VsUSq5FNl67vqDeOTjND0SVA'
    });
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 48.87917,
          lng: 2.15905
        },
        zoom: 15,
        tilt: 30
      }
    });
  }

}
