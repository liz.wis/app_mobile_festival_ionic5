import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginUser = {
    email: '',
    password: ''
  };

  isCaptchaValid = false;

  constructor(public afAuth: AngularFireAuth) { }

  get siteKey(){
    return environment.recaptcha.siteKey;
  }

  ngOnInit() {
  }


  captchaRresolved(){
    console.log("Captcha resolved");
    this.isCaptchaValid = true;
  }

  login(){
    console.log('email: ' + this.loginUser.email);
    console.log('password: ' + this.loginUser.password);
    this.afAuth.signInWithEmailAndPassword(this.loginUser.email, this.loginUser.password).then(auth=>{
      console.log('utilisateur connecté: ' + auth.user.uid);
    })
    .catch(error => {
      console.log ('utilisateur pas connecté');
    });
  }

  signIn(){
    this.afAuth.createUserWithEmailAndPassword(this.loginUser.email, this.loginUser.password);
    this.loginUser = {
      email: '',
      password: ''
    };
  }
}
